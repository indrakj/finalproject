<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    {{ $newUserStatus ? "Selamat datang di Aplikasi Project Management." : "" }} Ini adalah kode OTP Anda : {{ $otpCode->otp }}. Kode OTP ini berlaku 5 menit. Jangan berikan kode ini kepada siapapun.
</body>

</html>