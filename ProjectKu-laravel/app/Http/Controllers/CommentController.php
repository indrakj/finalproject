<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use App\Events\CommentStoredEvent;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->except(['index', 'show']);
    }
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table comment
        $comments = Comment::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data comment',
            'data' => $comments
        ], 200);
    }

    /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find comment by ID
        $comment = Comment::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Comment',
            'data' => $comment
        ], 200);
    }

    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'comment'   => 'required',
            'projects_id'  => 'required'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $comment = Comment::create([
            'comment'     => $request->comment,
            'projects_id'    => $request->projects_id
        ]);
        
        // //email untuk pemilik project
        // Mail::to($comment->project->user->email)->send(new ProjectAuthorMail($comment));

        // //email untuk pemilik comment
        // Mail::to($comment->user->email)->send(new CommentAuthorMail($comment));

        //memanggil event CommentStoredEvent
        event(new CommentStoredEvent($comment));

        //success save to database
        if($comment) {

            return response()->json([
                'success' => true,
                'message' => 'Comment Created',
                'data'    => $comment  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Comment Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $comment
     * @return void
     */
    public function update(Request $request, Comment $comment)
    {   
        //set validation
        $validator = Validator::make($request->all(), [
            'comment'   => 'required',
            'projects_id'  => 'required'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find comment by ID
        $comment = Comment::findOrFail($comment->id);

        if($comment) {

            $user = auth()->user();
            //check if only login user can delete the comment

            if($comment->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data komentar ini bukan milik user login'
                ], 403);
            }

            //update comment
            $comment->update([
            'comment'     => $request->comment,
            'projects_id'    => $request->projects_id
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Comment Updated',
                'data'    => $comment  
            ], 200);

        }

        //data comment not found
        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find comment by ID
        $comment = Comment::findOrfail($id);

        if($comment) {

            $user = auth()->user();
            //check if only login user can delete the comment

            if($comment->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data komentar ini bukan milik user login'
                ], 403);
            }

            //delete comment
            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Comment Deleted',
            ], 200);

        }

        //data comment not found
        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }
}
