<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProjectController extends Controller
{
    public function __construct()
    {   
        return $this->middleware('auth:api')->only(['store' , 'update' , 'delete']);
    }

    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table role
        $projects = Project::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Project',
            'data' => $projects
        ], 200);
    }

    /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find role by ID
        $project = Project::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Project',
            'data' => $project
        ], 200);
    }

    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'title'         => 'required',
            'description'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = auth()->user();

        //save to database
        $project = Project::create([
            'title'         => $request->title,
            'description'   => $request->description,
            'photo'         => $request->photo
        ]);

        //success save to database
        if($project) {

            return response()->json([
                'success' => true,
                'message' => 'Project Created',
                'data'    => $project  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Project Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $role
     * @return void
     */
    public function update(Request $request, Project $project)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'title'         => 'required',
            'description'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find project by ID
        $project = Project::findOrFail($project->id);

        if($project) {
            $user = auth()->user();

            if($project->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data project bukan milik user login',
                ] , 403);

<<<<<<< HEAD
            }

            //update 
=======
            $user = auth()->user();

            //update project
            if($project->users_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data project ini bukan milik user login'
                ], 403);
            }

            //update post
>>>>>>> 876b0614533ecafc3b4dd936e52863f078cd8fef
            $project->update([
                'title'     => $request->title,
                'description'   => $request->description
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Project Updated',
                'data'    => $project  
            ], 200);

        }

        //data  not found
        //data project not found
        return response()->json([
            'success' => false,
            'message' => 'Project Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find project by ID
        $project = Project::findOrfail($id);

        if($project) {
            $user = auth()->user();

            if($project->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data project bukan milik user login',
                ] , 403);

            }
            
            //delete project
            $project->delete();

            return response()->json([
                'success' => true,
                'message' => 'Project Deleted',
            ], 200);

        }

        //data proejct not found
        return response()->json([
            'success' => false,
            'message' => 'Project Not Found',
        ], 404);
    }
}
