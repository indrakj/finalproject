<?php

namespace App\Http\Controllers\Auth;
<<<<<<< HEAD
/*===onprocess===
use App\Events\OtpCodeStoredEvent;
*/
=======

>>>>>>> 876b0614533ecafc3b4dd936e52863f078cd8fef
use App\User;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
<<<<<<< HEAD
use App\Http\Controllers\Controller;
=======
use App\Events\AuthStoredEvent;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
>>>>>>> 876b0614533ecafc3b4dd936e52863f078cd8fef
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
<<<<<<< HEAD
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'name' => 'required',
            'email' => 'required|unique:users,email|email',
            'username' => 'required|unique:users,username'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::create($allRequest);

        do {
            $random = mt_rand( 100000 , 999999);
            $check = OtpCode::where('otp' , $random)->first();

        } while($check);
=======
        
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
            'email' => 'required|unique:users,email|email',
            'username' => 'required|unique:users,username',
            'roles_id' => 'required'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'roles_id' => $request->roles_id
        ]);

        do {
            $random = mt_rand(100000 , 999999);
            $check = OtpCode::where('otp' , $random)->first();
        } while ($check);
>>>>>>> 876b0614533ecafc3b4dd936e52863f078cd8fef

        $now = Carbon::now();

        $otp_code = OtpCode::create([
            'otp' => $random,
<<<<<<< HEAD
            'valid_until' =>    $now->addMinutes(5),
            'user_id' => $user->id
=======
            'valid_until' => $now->addMinutes(5),
            'users_id' => $user->id
        ]);

        event(new AuthStoredEvent($user));
        // Mail::to($user->email)->send(new RegisterMail($user));

        return response()->json([
            'success' => true,
            'message' => 'Data User berhasil dibuat',
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code
            ]
>>>>>>> 876b0614533ecafc3b4dd936e52863f078cd8fef
        ]);
/*===onprocess===
        event(new OtpCodeStoredEvent($otp_code , true));
*/
        //kirim email otp code ke email register

        return response()->json([
                'success' => true,
                'message' => 'Data User berhasil dibuat',
                'data' => [
                    'user' => $user,
                    'otp_code' => $otp_code
                ]
            ]);

    }
}
