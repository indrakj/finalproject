<?php

namespace App\Listeners;

use App\Mail\RegisterMail;
use App\Events\AuthStoredEvent;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailRegisterUser implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AuthStoredEvent  $event
     * @return void
     */
    public function handle(AuthStoredEvent $event)
    {
        // dd('masuk ke fungsi auth');
        Mail::to($event->user->email)->send(new RegisterMail($event->user));
    }
}
