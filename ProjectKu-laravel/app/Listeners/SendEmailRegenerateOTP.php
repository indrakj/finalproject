<?php

namespace App\Listeners;

use App\Mail\RegenerateOtpMail;
use App\Events\RegenerateOTPEvent;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailRegenerateOTP implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegenerateOTPEvent  $event
     * @return void
     */
    public function handle(RegenerateOTPEvent $event)
    {
        // dd('masuk ke sendemail');
        Mail::to($event->otp_code->user->email)->send(new RegenerateOtpMail($event->otp_code));
    }
}
