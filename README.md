<h2>Kelompok 16</h2>

<h2>Anggota Kelompok:</h2>
<p>Indra Komara Jayadi</p>
<p>Achmad Nur Fauzi</p>
<p>Abdullah Faqih Rachmat</p>

<h2>Tema Project:</h2>
<p>ProjectKu App - Aplikasi Untuk Memanage Projectmu</p>

<h2>ERD</h2>
<img src="https://gitlab.com/indrakj/finalproject/-/raw/master/erd.png">

<h2>Link Video:</h2>

<h2>Link Deploy:</h2>
<a href="http://projectkuapp.com/ProjectKu-laravel/">ProjectKu App laravel</a>
<a href="http://projectkuapp.com/projectku-vue/">ProjectKu App vue</a>

<h2>Link Publish Postman</h2>
<a href="https://documenter.getpostman.com/view/13639492/UVR7KTJ4">Postman</a>

